#!/usr/bin/env python3

import gwpy
from gwpy.timeseries import TimeSeriesDict as tsd
from gwpy.timeseries import TimeSeries as ts
from gwpy.segments import DataQualityFlag
from gwpy.time import tconvert
from gwpy.time import to_gps

import numpy as np
import os
import sys
import pandas as pd
import configparser
from optparse import OptionParser, Option
import json
import copy

from scipy.interpolate import interp1d
from scipy import signal

'''
The purpose of this script is to monitor if the calibration accuracy is staying within the expected 
calibration uncertainty bounds.  If it is not, a calibration measurements should be triggered so a new
calibration uncertainty can be established and/or the calibration models can be updated.
'''

#############################################################################
##################### Program Command Line Options ##########################
#############################################################################

parser = OptionParser(description = __doc__)

# Append program specific options

#parser.add_option("--gps-start-time", metavar = "seconds", help = "Set the start time of the segment to analyze in GPS seconds. This is required iff DataSource is frames")
#parser.add_option("--gps-end-time", metavar = "seconds", help = "Set the end time of the segment to analyze in GPS seconds. This is required iff DataSource is =frames")
parser.add_option("--output-path", metavar = "name", default = ".", help = "Set the output path for writing frame files. (Default=Current)")
parser.add_option("--config-file", metavar = "name", help = "Full path to configuration file for running.")

# Parse options
options, filenames = parser.parse_args()

#gpsstarttime = options.gps_start_time
#gpsendtime = options.gps_end_time
#duration = float(gpsendtime) - float(gpsstarttime)

now = int(to_gps("now"))
duration = 86400
gpsendtime = int(now - 1800)
gpsstarttime = int(gpsendtime - duration)


##############################################################################
############################# Parse Config File ##############################
##############################################################################

# Parse arguments from config file

def ConfigSectionMap(section):
	dict1 = {}
	options = Config.options(section)
	for option in options:
		try:
			dict1[option] = Config.get(section, option)
			if dict1[option] == -1:
				DebugPrint("skip: %s" % option)
		except:
			print("exception on %s!" % option)
			dict[option] = None
	return dict1

Config = configparser.ConfigParser()
Config.read(options.config_file)

InputConfigs = ConfigSectionMap("InputConfigurations")
OutputConfigs = ConfigSectionMap("OutputConfigurations")
DebuggingConfigs = ConfigSectionMap("DebuggingConfigurations")

freqs_from_channel = Config.getboolean("InputConfigurations", "freqsfromchannel")
verbose = Config.getboolean("DebuggingConfigurations", "verbose")
endstation = Config.getboolean("InputConfigurations", "endstation")
strain_from_calcs = Config.getboolean("InputConfigurations", "strainfromcalcs")
kappac=InputConfigs["kappac"]
fcc= InputConfigs["fcc"]
fccfileforrefvalue = InputConfigs["fccrefpath"]
fftlength = float(InputConfigs["fftlength"])
overlap = float(InputConfigs["overlap"])
window = InputConfigs["window"]
cadence = float(InputConfigs["cadence"])
default_minmag = float(InputConfigs["minmag"])
default_maxmag = float(InputConfigs["maxmag"])
default_minphase = float(InputConfigs["minphase"])
default_maxphase = float(InputConfigs["maxphase"])
default_mincoh = float(InputConfigs["mincoh"])
default_maxcoh = float(InputConfigs["maxcoh"])
interpmethod = InputConfigs["interpmethod"]
tag = InputConfigs["tag"]
#timeRef = float(InputConfigs["timeref"])
savedata = Config.getboolean("InputConfigurations", "savedata")
include_gpstimes_in_filename = Config.getboolean("OutputConfigurations", "gpstimesinfilename")
analyze_additional_pcal = Config.getboolean("InputConfigurations", "analyzeadditionalpcal")
outputdir = OutputConfigs["outputdir"]

##############################################################################
################################ Main Program ################################
##############################################################################

# Reading f_cc from the pydarm_H1.ini
conf = configparser.ConfigParser()
conf.read(fccfileforrefvalue)
fccmeanRef = conf["sensing"]["coupled_cavity_pole_frequency"]

#Setting kappa_ref = 1
kappmeanRef = 1

# Set up channel list and read in data
ifo = InputConfigs["ifo"]
chList = [(ifo, InputConfigs["strain"]), (ifo, InputConfigs["pcal"])]
if analyze_additional_pcal:
	chList.append((ifo, InputConfigs["additionalpcal"]))
if freqs_from_channel:
	oscfreqchans = json.loads(InputConfigs["oscillatorfrequencies"])
	oscchList = []
	for channel in oscfreqchans:
		chList.append((ifo, channel))
		oscchList.append((ifo, channel))
        # If analyzing a second pcal channel, load those freqs too
	if analyze_additional_pcal:
		additional_oscfreqchans = json.loads(InputConfigs["additionaloscillatorfrequencies"])
		additional_oscchList = []
		for channel in additional_oscfreqchans:
			chList.append((ifo, channel))
			additional_oscchList.append((ifo, channel))

# First grab the data quality segments as specified in the config file
if verbose:
	print("Gathering the data quality segments...")
dqsegsfile = 'dqsegs_' + tag + '_' + str(gpsstarttime) + '_' + str(gpsendtime) + '.h5'
if os.path.exists(InputConfigs["savesegspath"] + '/' + dqsegsfile):
	if verbose:
		print("Reading saved data segs file...")
	segs = DataQualityFlag.read(InputConfigs["savesegspath"] + '/' + dqsegsfile, InputConfigs["dqflag"])
else:
	segs = DataQualityFlag.query(InputConfigs["dqflag"], float(gpsstarttime), float(gpsendtime))
	if savedata:
		segs.write(InputConfigs["savesegspath"] + '/' + dqsegsfile)
if verbose:
	print("Got the data quality segments...")

# Create empty output dictionaries
tf_mag = {}
tf_phase = {}
coh = {}
navg = {}
stime = {}
etime = {}
tmag = {"freq":[], "mag":[], "phase":[], "coherence":[], "n_avg":[], "starttime":[], "endtime": []}
#tmag = {}
oscfreqs = []
freqlist = []
if analyze_additional_pcal:
	additional_oscfreqs = []

#if not segs.active:
#	sys.exit("There are no times where specified data quality flag is active on this day.")

# Loop over times where the specified data quality flag is active
n_avg = []
for seg in segs.active:
	seg_duration = int(seg[1])-int(seg[0])
	num = int(seg_duration/fftlength)
	if num>=1:
		n_avg.append(num)
	if seg[1]-seg[0] < fftlength:
		print("Segment %d - %d is too short for a median duration of %f. Skipping..." % (seg[0], seg[1], fftlength))
		continue
	if verbose:
		print("Gathering the data for GPS times %d - %d..." % (int(seg[0]), int(seg[1])))
	datafile = 'data_' + tag + '_' + '%d' % int(seg[0]) + '_' + '%d' % int(seg[1]) + '.h5'
	if os.path.exists(InputConfigs["savedatapath"] + '/' + datafile):
		if verbose:
			print("Reading saved data file for GPS times %d - %d: %s" % (int(seg[0]), int(seg[1]), InputConfigs["savedatapath"] + '/' + datafile))
		data = tsd.read(InputConfigs["savedatapath"] + '/' + datafile, list(map("%s:%s".__mod__,chList)), verbose=verbose)
	else:
		data = tsd.get(list(map("%s:%s".__mod__,chList)), seg[0], seg[1], verbose=verbose)
		if savedata:
			data.write(InputConfigs["savedatapath"] + '/' + datafile)
	if verbose:
		print("Got the data for GPS times %d - %d..." % (int(seg[0]), int(seg[1])))

	strain = data["%s:%s" % (ifo, InputConfigs["strain"])]
	pcal = data["%s:%s" % (ifo, InputConfigs["pcal"])]
	if analyze_additional_pcal:
		additional_pcal = data["%s:%s" % (ifo, InputConfigs["additionalpcal"])]

	# Create a list of injection frequencies either from channels containing the oscillator frequencies
	# or from a list provided in the config file 
	if freqs_from_channel:
		freq_list = []
		for chan in list(map("%s:%s".__mod__,oscchList)):
			freq_ts = data[chan]
			# Check if the oscillator frequency changes at all during the analysis time period
			check = np.full(freq_ts.value.shape, freq_ts.value[0])
			is_same = np.all((np.abs(freq_ts.value-check) < 1e-12))
			if is_same:
				# If it doesn't, can just grab the first frequency in the time series
				freq_list.append(freq_ts.value[0])
				starttime = seg[0] 
				endtime = seg[1]
			else:
				# If it does, for now we will exit and throw an error.  
				# FIXME: Could add some behavior to deal with changing oscillator frequencies at some point...
				print ("Oscillator frequency given in channel %s has changed over integration time period." % data[chan])
				for x in range(len(freq_ts)):
					if freq_ts[x]!= freq_ts[x-1]:
						starttime2 = int(seg[0]) + (x/16)
						endtime2 = int(seg[1])
						if (endtime2-starttime2) < fftlength:
							print("Segment %d - %d is too short for a median duration of %f. Skipping..." %(starttime2, endtime2, fftlength))
							continue
						else:
							freq_list.extend([freq_ts.value[-1]])
					else:
						starttime1 = int(seg[0]) 
						endtime1 = int(seg[0]) + (x/16)
						if (endtime1-starttime1) < fftlength:
							print("Segment %d - %d is too short for a median duration of %f. Skipping..." % (starttime2, endtime2, fftlength))
							continue
						else:
							freq_list.append(freq_ts.value[0])
		if analyze_additional_pcal:
			additional_freq_list = []
			for chan in list(map("%s:%s".__mod__,additional_oscchList)):
				additional_freq_ts = data[chan]
				# Check if the oscillator frequency changes at all during the analysis time period
				check = np.full(additional_freq_ts.value.shape, additional_freq_ts.value[0])
				is_same = np.all(((additional_freq_ts.value-check) < 1e-12))
				if is_same:
					# If it doesn't, can just grab the first frequency in the time series
					additional_freq_list.append(additional_freq_ts.value[0])
				else:
					print("Oscillator frequency given in channel %s has changed over integration time period." % data[chan])
					additional_freq_list.append(additional_freq_ts.value[-1])
	else:
		freq_list = json.loads(InputConfigs["oscillatorfrequencies"])
		if analyze_additional_pcal:
			additional_freq_list = json.loads(InputConfigs["additionaloscillatorfrequencies"])
	
	oscfreqs = np.sort(np.array(freq_list))
	if oscfreqs[0] not in freqlist:
		freqlist.append(oscfreqs[0]) 	
	if analyze_additional_pcal:
		additional_oscfreqs = np.sort(np.array(additional_freq_list))
	# Create dictionary keys for each oscillator frequency, if this hasn't been done yet
	# FIXME: This will fail if the oscillator frequencies have changed at all between lock stretches
	for freq in freqlist:
		tf_mag[freq] = []
		tf_phase[freq] = []
		coh[freq] = []	
		navg[freq] = []
		stime[freq] = []
		etime[freq] = []
	if analyze_additional_pcal:
		for freq in additional_oscfreqs:
			tf_mag[freq] = []
			tf_phase[freq] = []
			coh[freq] = []

	#
	# Use gwpy to compute the (uncorrected) transfer function: https://gwpy.github.io/docs/latest/api/gwpy.timeseries.TimeSeries/#gwpy.timeseries.TimeSeries.transfer_function
	#
	if verbose:
		print("Computing transfer function at oscillator frequencies...")

	# First, make sure data is stored as gwpy timeseries
	pcal = ts(pcal, dtype=float)
	strain = ts(strain, dtype=float)
	if analyze_additional_pcal:
		additional_pcal = ts(additional_pcal)
	
	# Create a temporary dictionary for storing results for this lock stretch only
	tf_mag_tmp = {}
	tf_phase_tmp = {}
	coh_tmp = {}
	for freq in oscfreqs:
		tf_mag_tmp[freq] = []
		tf_phase_tmp[freq] = []
		coh_tmp[freq] = []
		if analyze_additional_pcal:
			for freq in additional_oscfreqs:
				tf_mag_tmp[freq] = []
				tf_phase_tmp[freq] = []
				coh_tmp[freq] = []
        
	# Compute the coherence for this segment
	# I need to median the coherence across all given segments by hand since this is not an option for the coherence method
	fft_start = float(seg[0])
	fft_end = fft_start + fftlength
	data2 = tsd.get(["%s:%s"%(ifo,kappac),"%s:%s"%(ifo,fcc)], fft_start, fft_end)
	kapp = data2["%s:%s"%(ifo,kappac)]
	fccc = data2["%s:%s"%(ifo,fcc)]
	kappmean = np.mean(kapp.value[:])
	fccmean = np.mean(fccc.value[:])
	# Create temporary array for storing coherence median results
	coh_median_array = []
	if analyze_additional_pcal:
		coh_additional_median_array = []
	while fft_end <= float(seg[1]):
		# Crop the data to fit the fftlength for this iteration
		fft_pcal_seg = pcal.crop(fft_start, fft_end)
		fft_strain_seg = strain.crop(fft_start, fft_end)
		if analyze_additional_pcal:
			fft_additional_pcal_seg = additional_pcal.crop(fft_start, fft_end)
		
		if (fft_pcal_seg.duration.value < fftlength) or (fft_strain_seg.duration.value < fftlength):
			print("This FFT segment is only %f long and we need %f for each FFT.  Moving on..." % (min(fft_pcal_seg.duration.value, fft_strain_seg.duration.value), fftlength))
			break
		if analyze_additional_pcal:
			if (fft_additional_pcal_seg.duration.value < fftlength):
				print("This FFT segment is only %f long and we need %f for each FFT.  Moving on..." % (fft_additional_pcal_seg.duration.value, fftlength))
				break

		coh_fft_seg = fft_pcal_seg.coherence(fft_strain_seg, fftlength=fftlength/2, overlap = fftlength/4, window=window)
		coh_fft_seg_freqs = np.asarray(coh_fft_seg.frequencies)
		index = np.where((coh_fft_seg_freqs>oscfreqs[0]-0.001) & (coh_fft_seg_freqs<oscfreqs[0]+0.001))
		coh_fft_seg_at_oscfreqs = np.array([np.asarray(coh_fft_seg.value)[i] for i in index])
		coh_median_array.append(np.mean(coh_fft_seg_at_oscfreqs[coh_fft_seg_at_oscfreqs>0.0]))
		if analyze_additional_pcal:
			coh_additional_fft_seg = fft_additional_pcal_seg.coherence(fft_strain_seg, fftlength=fftlength, window=window)
			coh_additional_fft_seg_freqs = np.asarray(coh_additional_fft_seg.frequencies)
			coh_additional_fft_seg_at_oscfreqs = np.median(np.asarray(coh_additional_fft_seg))
			coh_additional_median_array.append(coh_additional_fft_seg_at_oscfreqs)
		fft_start += cadence
		fft_end = fft_start + fftlength

	# Compute the transfer function for this segment
	csd_seg = fft_pcal_seg.csd(fft_strain_seg, fftlength=fftlength, overlap=overlap, window=window, average='mean')
	psd_seg = fft_pcal_seg.psd(fftlength=fftlength, overlap=overlap, window=window, average='mean')
	uncorrected_tf_seg = csd_seg / psd_seg
	tf_freqs = np.asarray(uncorrected_tf_seg.frequencies)
	# Set up transfer function interpolator
	uncorrected_tf_seg_interp = interp1d(tf_freqs, uncorrected_tf_seg, kind=interpmethod)
	tf_at_oscfreqs = uncorrected_tf_seg_interp(oscfreqs)        
	if analyze_additional_pcal:
		additional_csd_seg = fft_additional_pcal_seg.csd(fft_strain_seg, fftlength=fftlength, overlap=overlap, window=window, average='mean')
		additional_psd_seg = fft_additional_pcal_seg.psd(fftlength=fftlength, overlap=overlap, window=window, average='mean')
		additional_uncorrected_tf_seg = additional_csd_seg / additional_psd_seg
		additional_tf_freqs = np.asarray(additional_uncorrected_tf_seg.frequencies)
		# Set up transfer function interpolator
		additional_uncorrected_tf_seg_interp = interp1d(additional_tf_freqs,
                                                                additional_uncorrected_tf_seg, kind=interpmethod)
		additional_tf_at_oscfreqs = additional_uncorrected_tf_seg_interp(additional_oscfreqs)
	# Store final values for this segment in temporary dictionaries
	for i, freq in enumerate(oscfreqs):
		corrFactor = kappmeanRef / kappmean * (1+freq/fccmean*1j) / (1+freq/float(fccmeanRef)*1j)
		tf_mag_tmp[freq].append([fft_start, np.abs(tf_at_oscfreqs[i]*corrFactor)])
		tmag["freq"].append(freq)
		tmag["mag"].append(tf_mag_tmp[freq][0][1])
		tf_phase_tmp[freq].append([fft_start, np.angle(tf_at_oscfreqs[i]*corrFactor)])
		tmag["phase"].append(tf_phase_tmp[freq][0][1])
		coh_tmp[freq].append([fft_start, coh_median_array[i]])
		tmag["coherence"].append(coh_tmp[freq][0][1])
		tmag["n_avg"] = n_avg
		tmag["starttime"].append(int(seg[0]))
		tmag["endtime"].append(int(seg[1]))
	if analyze_additional_pcal:
		for i, freq in enumerate(additional_oscfreqs):
			tf_mag_tmp[freq].append([fft_start,
                                                 np.abs(additional_tf_at_oscfreqs[i]*corrFactor)])
			tf_phase_tmp[freq].append([fft_start,
                                                   np.angle(additional_tf_at_oscfreqs[i]*np.abs(corrFactor))])
			coh_tmp[freq].append([fft_start,
                                              coh_additional_fft_seg_at_oscfreqs[i]])

	# Add results from this lock stretch to the dictionary for final results
	
	c = 0
	for freq in freqlist:
		f = [i for i in tmag["freq"] if i == freq]
		tf_mag[freq].append(np.mean(tmag["mag"][c:len(f)+c]))
		tf_phase[freq].append(np.mean(tmag["phase"][c:len(f)+c]))
		coh[freq].append(np.mean(tmag["coherence"][c:len(f)+c]))
		navg[freq].append(2*(sum(tmag["n_avg"][c:len(f)+c]))-1)
		stime[freq].append(tmag["starttime"][c:len(f)+c])
		etime[freq].append(tmag["endtime"][c:len(f)+c])

# Check if no active lock stretches were long enough to compute anything
try:
	oscfreqs
except:
	sys.exit("No active data quality segment stretches were long enough to compute anything.")




for i in freqlist:
	with open(outputdir + '/DARM_OVER_PCAL_HF_roaming_%s.txt'%round(i), 'a+') as f:
		f.seek(0)
		first_line = f.read().split('\n')[0]
		if first_line == 'Freq, TF_mag, TF_phase(deg), coh, n_avg, start_time, end_time ':
			t_mag = tf_mag[i][0]	
			t_ph = tf_phase[i][0]
			chrnce = coh[i][0]
			n = navg[i][0]
			st = stime[i][0][0]
			et = etime[i][0][-1]
			line = "%.2f, %.8f, %.8f, %.8f, %.1f , %i, %i\n"%(i,t_mag,t_ph,chrnce,n,st,et)
			f.write(line)
		else:
			f.write("Freq, TF_mag, TF_phase(deg), coh, n_avg, start_time, end_time \n")
			t_mag = tf_mag[i][0]
			t_ph = tf_phase[i][0]
			chrnce = coh[i][0]
			n = navg[i][0]
			st = stime[i][0][0]
			et = etime[i][0][-1]
			line = "%.2f, %.8f, %.8f, %.8f, %.1f , %i, %i\n"%(i,t_mag,t_ph,chrnce,n,st,et) 
			f.write(line)
