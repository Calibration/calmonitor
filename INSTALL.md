# Set up environment

Run an appropriate environment script:

```shell
INSTALLPATH=/path/to/install

PATH=${INSTALLPATH}/bin:$PATH
PKG_CONFIG_PATH=${INSTALLPATH}/lib/pkgconfig:${INSTALLPATH}/lib64/pkgconfig:$PKG_CONFIG_PATH
PYTHONPATH=${INSTALLPATH}/lib/python3.9/site-packages:${INSTALLPATH}/lib64/python3.9/site-packages
GST_PLUGIN_PATH=${INSTALLPATH}/lib/gstreamer-1.0:${INSTALLPATH}/lib64/gstreamer-1.0:$GST_PLUGIN_PATH
GST_REGISTRY_1_0=${INSTALLPATH}/registry.bin

export PATH PKG_CONFIG_PATH PYTHONPATH GST_PLUGIN_PATH GST_REGISTRY_1_0
```

# Installing dependencies

Start in an `igwn-py39` conda environment
```shell
conda activate igwn-py39
```

Clone the `gstlal` git repository, check out the correct branch, and install the `gstlal` package:
```shell
git clone git@git.ligo.org:lscsoft/gstlal.git
cd gstlal
git checkout calib-mon-compatible
cd gstlal
./00init.sh
./configure --prefix=/path/to/install --without-doxygen
make
make install
```



